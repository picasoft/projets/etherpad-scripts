# Archives Etherpad

Cette image Docker est un simple Nginx qui sert les pages HTML d'anciens pads exportés.

## Préparation

On récupère tout les pads au format HTML

```bash
ETHERPAD_API_KEY="WRITE_ETHERPAD_API_TOKEN_HERE" python3 export_all_html.py
```

Ce script, un peu long, va remplir le dossier `./export` avec les pages HTML à servir. Comme le volume reste assez faible, on va les intégrer directement dans l'image Docker.
