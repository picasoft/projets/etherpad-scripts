#!/usr/bin/python3
# coding=utf-8

"""Export all Etherpad instance to HTML."""

# Imports
import os
import sys
import requests

# Constants
EXPORT_FOLDER = os.getenv('EXPORT_FOLDER', './export')
ETHERPAD_API = os.getenv('ETHERPAD_API', 'https://pad.picasoft.net/api/1.2.13')
ETHERPAD_API_KEY = os.getenv('ETHERPAD_API_KEY')
if not ETHERPAD_API_KEY:
    print('ETHERPAD_API_KEY environment variable mus bet set')
    sys.exit(-1)


def api_request(path, verb="GET", parameters={}):
    """
    Send a request to Etherpad API.
    """
    static_parameters = {
        'apikey': ETHERPAD_API_KEY
    }
    try:
        res = requests.request(
            verb,
            url=ETHERPAD_API + path,
            params={**static_parameters, **parameters}
        )
        data = res.json()
    except Exception as err:
        print(err)
        sys.exit(2)
    return data['data']


def main():
    """Run the main function."""
    try:
        os.mkdir(EXPORT_FOLDER)
    except FileExistsError:
        pass

    pads = api_request("/listAllPads")['padIDs']
    for pad in pads:
        if os.path.isfile(EXPORT_FOLDER + '/' + pad + '.html'):
            continue
        pad_obj = api_request(path="/getHTML", parameters={'padID': pad})
        if not pad_obj:
            print('UNABLE TO EXPORT PAD ' + pad)
            continue
        content = pad_obj['html']
        with open(EXPORT_FOLDER + '/' + pad + '.html', 'w') as outfile:
            outfile.write(content)


if __name__ == '__main__':
    main()
